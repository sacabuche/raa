from django.contrib import admin
from catalog.models import Catalog, CatalogImage

class CatalogImageInline(admin.StackedInline):
    model = CatalogImage
    extra = 0

class CatalogAdmin(admin.ModelAdmin):
    inlines = (CatalogImageInline,)
    #list_display = ('name', 'get_url')
    list_display = ('name',)

admin.site.register(Catalog, CatalogAdmin)

