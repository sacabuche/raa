/*(function($){
	$.fn.simplecarousel = function(){
	return this
	};
})(jQuery);*/


function simplecarousel($obj){
	var LEFT = 0,
	    RIGHT = 1,
	    /* FIXME: to use multiple simplecarousel for page this must be moved */
	    IMAGES_TO_SCROLL = $obj.attr('data-to-scroll') || 5,
	    IMAGES_DISPLAYING = $obj.attr('data-displaying') || 5,
	    AUTOSLIDE = $obj.attr('data-autoslide') || 0,
	    MOUSE_FELLOW = $obj.attr('data-mouse-fellow') || false,
	    SHOW_TEXT = $obj.attr('data-show-text') || true,
	    /* FIXME  END*/
	    INNER_WRAPPER_NAME = 'simple-carousel-inner-wrapper',
	    EMPTY_IMAGE = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
	    $background,
            $b_left_arrow, $b_right_arrow,
	    background_img,
            $index_position,
	    $text, $close_text,
	    timer,
	    autoslide_dir = RIGHT;

	IMAGE_TO_SCROLL = parseInt(IMAGES_TO_SCROLL, 10);
	IMAGES_DISPLAYING = parseInt(IMAGES_DISPLAYING, 10);
	AUTOSLIDE = parseFloat(AUTOSLIDE, 10);

	function set_timer(options){
		if (AUTOSLIDE === 0){
		return
		}
		timer = window.setInterval(function(){
			autoslide(options);
		}, AUTOSLIDE*1000);
	};

	function on_load_image(e){
		var background_img = this;
		$(background_img).show();
		$(background_img).attr('style', '');
		function get_size(obj){
			return {width:$(obj).width(),
				height: $(obj).height()
			}
		};

		var win = get_size(window),
		    img = get_size(background_img);

		if (win.width < img.width){
			$(background_img).width(win.width*.8);
			img = get_size(background_img);
		}

		if (win.height < img.height){
			$(background_img).height(win.height * 0.8);
			img = get_size(background_img);
		}

		var left = (win.width - img.width)/2;
		var top = (win.height - img.height)/2;
		/* Set the position for the elements in the correct position */
		$(background_img).css({
			'top':top,
			'left':left
		});
		$text.css({
			'top': top + $(background_img).height(),
			'left': left,
			'width':   $(background_img).width()
		});

		$close_text.css({
			'top': top - $close_text.height()/2,
			'left': left + $(background_img).width() -  $close_text.width()/2
		});

		if (SHOW_TEXT && background_img.alt !== ""){
			$text.show();
			$text.html(background_img.alt);
		}
                var img_index = $(background_img).attr('data-group-index');
                var img_total = $(background_img).attr('data-group-total');

                if (img_total !== '' && img_total !== '0' && img_total !== undefined){
                    img_index = parseInt(img_index, 10) + 1;
                    img_total = parseInt(img_total, 10) + 1;
                    var index_text = img_index + '/' + img_total;
                    $index_position.html(img_index + '/' + img_total).
                            css({'top':top -  $index_position.height() - 2,
                                 'left':left + $(background_img).width()/2 - $(index_text).width()/2}).
                            show();
	            $b_left_arrow.css({
				'top': top + $(background_img).height()/2,
				'left': 15
	            }).show();
		    $b_right_arrow.css({
				'top': top + $(background_img).height()/2,
				'right': 15
		    }).show();
                }

		/* Must be renamed to close_button */
		$close_text.show();
		$background.css('background-image','none');
	};
       
        function on_click_big_image(e){
            next_big_image();
        };

	function next_big_image(direction){
                var delta = direction || 1;
		$background.css('background-image', '');
		var $img = $(background_img),
		    index = $img.attr('data-group-index'),
		    group = $img.attr('data-group'),
		    $images;
		if (group === ''){
			/*TODO: Avance to the next group of photos */
			return
		}

		$images = $('ul [data-group='+group+']');
		index = parseInt(index, 10) + delta;
                var limit = $images.size();
		if (index >= limit){
			index = 0;
		}
                else if(index < 0){
                        index = limit -1;
                }
		/* this is suposed to clear the image */
		$img.attr('src', EMPTY_IMAGE);
		$img.hide();
		$($images.get(index)).find('img').click();
	};

	function create_background_img(){
		var background_img = document.createElement('img');
		$(background_img).
			attr('id', 'simple-carousel-big-image').
			appendTo(document.body).
			load(on_load_image).
			click(on_click_big_image).
			hide().
			addClass('rounded-corners');
		return  background_img
	};

	background_img = create_background_img();

	$text = $(document.createElement('p')).
		attr('id', 'simple-carousel-big-image-text').
		appendTo(document.body).
		addClass('rounded-corners').
		hide();
	$close_text = $(document.createElement('div')).
		attr('id', 'simple-carousel-close-text').
		appendTo(document.body).
		hide().click(close_background);
        $index_position = $(document.createElement('p')).
                attr('id', 'simple-carousel-index-position').
                appendTo(document.body).
                hide();

	function on_click_image(e){
		e.preventDefault();
		var $anchor =  $(e.target).closest('a');
		var $li = $(e.target).closest('li');
		var group = $li.attr('data-group');
		var group_index = $li.attr('data-group-index')
                var group_total = $li.attr('data-group-total'); 
		var href = $anchor.attr('href');
		var alt = $anchor.attr('data-alt');
		var background = e.data.background;
		background.show(100);
		if ($(background_img).attr('src') === EMPTY_IMAGE){
 			bind_unbind_esc();
		}
		$(background_img).attr('src', href).attr('alt', alt).
			attr('data-group', group).
                        attr('data-group-total', group_total).
			attr('data-group-index', group_index);
	};

	var can_click = true;
	function on_click_arrow(e){
		if (!can_click){
			return
		}
		can_click = false;
		
		var data = e.data;
		var scroller = data.to_scroll,
		    direction = data.direction,
		    image_width = data.image_width,
		    posible_offset = data.posible_offset,
		    offset,
		    position;
		
		position = scroller.scrollLeft();
		offset = (image_width * IMAGES_TO_SCROLL);
		if (direction == LEFT){
			position -= offset;
		}
		else{
			position += offset;
		}
		
		/*Change the direction of the autoslide*/
		autoslide_dir = direction;
		/*This is necessary for adjusting the speed of scrollLeft*/
		if (position >= posible_offset){
			position = posible_offset;
			autoslide_dir = LEFT;
			/* TODO: disable arrow*/
		}
		if (position <= 0){
			position = 0;
			autoslide_dir = RIGHT;
			/* TODO: disable arrow*/
		}
		scroller.animate({'scrollLeft':position}, function(){
			can_click = true;
		});
		window.clearInterval(timer);
		set_timer(data);
	};

	function set_arrow(obj, classes, direction, options){
		var the_options = jQuery.extend( true, {}, options);
		the_options.direction = direction;
		$(obj).addClass(classes).
		       click(the_options, on_click_arrow);
	};

	function create_arrows($wrapper, $inner_wrapper, image_width, total_width){
		var left_arrow = document.createElement('div');
		var right_arrow = document.createElement('div');

		var posible_offset = total_width - image_width * IMAGES_DISPLAYING;
		
		/*Set Events for click on arrows*/
		var options = {
			'to_scroll':$inner_wrapper,
			'image_width': image_width,
			'total_width': total_width,
			'posible_offset': posible_offset,
			'arrows': {
				'left': left_arrow,
				'right': right_arrow
			}};

		set_arrow(left_arrow,
			   'simple-carousel-left-arrow simple-carousel-arrow',
			   LEFT,
			   options);
		set_arrow(right_arrow,
			  'simple-carousel-right-arrow simple-carousel-arrow',
			  RIGHT,
			  options);

		/*Add the arrows for scroll*/
		$wrapper.append(left_arrow).append(right_arrow);
		set_timer(options);

		if (MOUSE_FELLOW){
		var width =  $inner_wrapper.width();
		var scroll_size = total_width - width;
		if (scroll_size >= 0) {
			$inner_wrapper.mousemove(
					{'scroll_size':scroll_size,
					 'width':width},
					function(e){
				var pos = (e.data.scroll_size * (e.pageX- $(this).offset().left)) / e.data.width;
				$(this).scrollLeft(pos);
			});
		}}

	};

	function adjust_width(img, img_q){
		var total_width,
		    image_width;
		    /*get necesary objects*/
		    $li = $(img).closest('li'),
		    $ul = $li.closest('ul');

		/*calculate new width*/
		image_width = $li.outerWidth();
		total_width = image_width * img_q;
		/*set new width*/
		$ul.width(total_width);
		return {'image':image_width, 'total':total_width}
	};

	function close_background(){
                $b_left_arrow.hide();
                $b_right_arrow.hide();
		$close_text.hide();
                $index_position.hide();
		$background.hide(200);
		$background.css('background', '');
		$text.hide().html("");
		$(background_img).remove();
		background_img = create_background_img();
 		bind_unbind_esc();
	};


	var esc_active = false;
	function bind_unbind_esc(){
		function esc_key(e){
			if (background_img.src === EMPTY_IMAGE){
			return
			}
			if (e.keyCode == 27){
				close_background();
			}
		};

		if (esc_active){
			$(document).unbind('keydown.simplec');
		}
		else{
			$(document).bind('keydown.simplec', esc_key);
		}
		esc_active = (!esc_active);
	};


	function autoslide(options){

		if (autoslide_dir === LEFT){
			$(options.arrows.left).click();
		}
		else{
			$(options.arrows.right).click();
		}
	};

	
	/* hide all the images of the group except the first finded
	 * Does not hide images with out group */
	function hide_image_groups(images_list){
		var groups = {}, group,
                    li_list = [];
		    NO_GROUP = '';
		$(images_list).find('li').each(function(){
			group = $(this).attr('data-group');
			if (group === NO_GROUP || group === undefined){
				return
			}
			if (group in groups){
				groups[group] += 1;
				$(this).hide().addClass('simple-carousel-hidden');
			}
			else{
				groups[group] = 0;
			}
			$(this).attr('data-group-index', groups[group]);
                        li_list.push(this);
		});
		/* add the total of images of this group*/
                var $li;
                for (var i = 0; i < li_list.length; i++){
                    $li = $(li_list[i]);
                    group = $li.attr('data-group');
                    total = groups[group];
                    $li.attr('data-group-total', total); 
		}
	};
	

	/*create background opaque*/
	$background = $(document.createElement('div')).
			attr('id','simple-carousel-bigbackground').
			append(document.createElement('div'));
	/*add to body*/
	$(document.body).append($background[0]);
	$background.click(close_background);
        
        /* background arrows */
        $b_left_arrow = $(document.createElement('div')).
                        attr('id', 'simple-carousel-b-left');
        $b_right_arrow = $(document.createElement('div')).
                        attr('id', 'simple-carousel-b-right');
        $(document.body).append($b_left_arrow);
        $(document.body).append($b_right_arrow);

        $b_right_arrow.click(function(e){
             next_big_image();
        });

        $b_left_arrow.click(function(e){
             next_big_image(-1);
        });


	$obj.each(function(){
		var $images,
		    $inner_wrapper,
	            $wrapper;

		hide_image_groups(this);

		/* create the super wrapper where arrows will be added */
		$(this).wrap('<div class="simple-carousel-wrapper" />');

		/* add wrapper where will scroll */
		
		$inner_wrapper = $(this).
		 		      wrap('<div class="'+INNER_WRAPPER_NAME+'" />').
		                      closest('.'+INNER_WRAPPER_NAME);

		/*Set thw width of inner Wrapper and create the arrows*/
		$wrapper = $(this).closest('.simple-carousel-wrapper');
		/* just select the photos that are visible (just one per group) 
		 * when grouping set a class named='simple-carousel-hidden' 
		 * this is needed because in adjust_width we pass the size,
		 * there shuld be a better way to do this an elegant one*/
		$images = $(this).find('li:not(.simple-carousel-hidden)').find('img');
		var img = $($images)[0];

		/*Verify if is on cache or already loaded*/
		if (img.complete){
			var width = adjust_width(img, $images.size());
			create_arrows($wrapper, $inner_wrapper,
					width.image, width.total);
		}
		/*Wait to load for execute the code*/
		else{
			$(img).load(function(){
				var width = adjust_width(img, $images.size());
				create_arrows($wrapper, $inner_wrapper,
						width.image, width.total);
			});
		}

		/* zoom for images that are enveloped by an anchor */
		$(this).find('a img').click({background:$background}, on_click_image);


	});

	/*create background opaque*/
	var background = document.createElement('div');


}

$(document).ready(function(){
    simplecarousel($('#carousel'));
})


