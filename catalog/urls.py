# This file is not used because we need only 2 fixed url look the main urls.py
from django.conf.urls.defaults import *
from django.contrib import admin
from catalog.views import CatalogDetailView, CatalogView


admin.autodiscover()

urlpatterns = patterns('',
    #url(r'^$', CatalogView.as_view(), name='catalog_all'),
    #url(r'^(?P<slug>[-\w]+)/$', CatalogDetailView.as_view(), name='catalog'),
    #url(r'^marcas/$', CatalogDetailView.as_view(), name='catalog_marcas',
        kwargs={'slug':'marcas'}),

)
