#from django.shortcuts import render_to_response
#from django.template import RequestContext
from catalog.models import Catalog
from django.views.generic import DetailView
from django.views.generic.base import View, TemplateResponseMixin

class CatalogDetailView(DetailView):

    context_object_name = "catalog"
    queryset = Catalog.objects.all()
    # default template
    # template_name = "catalog/catalog_detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CatalogDetailView, self)\
                .get_context_data(*args, **kwargs)

        context['images'] = self.object.images.all()
        return context

class CatalogView(TemplateResponseMixin, View):
    """
    Display all the calagos except 'marcas'
    """

    template_name = "catalog/all.html"

    def get(self, request):
        catalogs = Catalog.objects.select_related().all().exclude(slug='marcas')
        return self.render_to_response({'catalogs':catalogs})

