$(document).ready(function(){
  adjust_window();
  set_current_page();
});


function adjust_window(){
  var wrapper = $('#super-wrapper');
  var content_heigth = $('#page-wrapper').height();
 
  function adjust(){
    var doc_heigth = $(window).height() - content_heigth;
    var v_margin = doc_heigth/2;
    if (v_margin <= 0){
      $(wrapper).css('margin', "0 0");
    }
    else{
      $(wrapper).css('margin', v_margin + "px 0");
    }
  }
	
  adjust();
  $(window).resize(adjust);
}

function set_current_page(){
  var current_path = location.pathname;
  $('a[href="'+current_path+'"]').closest('li').addClass('current-page');


}
