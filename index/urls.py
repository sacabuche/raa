from django.conf.urls.defaults import *
from django.views.generic import TemplateView
from index.views import IndexView


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index',
    ),
)
