from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.conf import settings

from catalog.views import CatalogDetailView, CatalogView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^', include('raa.index.urls')),
    url(r'^contacto/', include('raa.contact_form.urls')),
    url(r'^productos/$', CatalogView.as_view(), name='catalog_all'),
    url(r'^marcas/$', CatalogDetailView.as_view(), name='catalog_marcas',
        kwargs={'slug':'marcas'}),


    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
     url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static
    try:
        import rosetta
    except ImportError:
        pass
    else:
        urlpatterns += (url(r'^rosetta/', include('rosetta.urls')),)
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
